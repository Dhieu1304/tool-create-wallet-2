const mongoose = require('mongoose');

const etherSchema = mongoose.Schema(
  {
    address: {
      type: String,
      required: true,
      index: true,
    },
    privateKey: {
      type: String,
      required: true,
      index: true,
    },
  },
  {
    timestamps: true,
  }
);

const EtherModel = mongoose.model('Ether', etherSchema);

module.exports = EtherModel;
