const mongoose = require('mongoose');

const tronSchema = mongoose.Schema(
  {
    address: {
      type: String,
      required: true,
      index: true,
    },
    privateKey: {
      type: String,
      required: true,
      index: true,
    },
  },
  {
    timestamps: true,
  }
);

const TronModel = mongoose.model('Tron', tronSchema);

module.exports = TronModel;
